require "application_system_test_case"

class SectionsTest < ApplicationSystemTestCase
  setup do
    @section = sections(:math_section).decorate
  end

  test "visiting the index" do
    visit sections_url
    assert_selector "h1", text: "Sections"
  end

  test "creating a Section" do
    visit sections_url
    click_on "New Section"

    select "MathRoom", from: 'section_classroom_id'
    select "Wednesday", from: 'section_days'

    fill_in "Starts at", with: '0900AM'
    fill_in "Ends at", with: '1000AM'
    select "English Teacher", from: 'section_teacher_id'
    click_on "Create Section"

    assert_text "Section was successfully created"
    click_on "Back"
  end

  test "updating a Section" do
    visit sections_url
    click_on "Edit", match: :first

    select "MathRoom", from: "section_classroom_id"
    select "Wednesday", from: 'section_days'
    fill_in "Ends at", with: @section.ends_at
    fill_in "Starts at", with: @section.starts_at
    select "Math Teacher", from: 'section_teacher_id'
    click_on "Update Section"

    assert_text "Section was successfully updated"
    click_on "Back"
  end

  test "destroying a Section" do
    visit sections_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Section was successfully destroyed"
  end
end
