require 'test_helper'

class SectionTest < ActiveSupport::TestCase
  def setup
    @one = sections(:math_section).decorate
    @two = sections(:english_section).decorate
  end

  test "human days of week" do
    assert_equal 'Monday / Tuesday / Wednesday', @one.days_human
    assert_equal 'Friday / Sunday', @two.days_human
  end

  test 'round seconds' do
    assert_equal '07:00:00', @one.starts_at.to_s
    assert_equal '09:26:00', @one.ends_at.to_s
  end

  test '#human_starts' do
    assert_equal '07:00', @one.human_starts
  end

  test '#human_ends' do
    assert_equal '09:26', @one.human_ends
  end

  test 'days of week only in 1..7 range' do
    section = Section.new days: [1, 2, 8]
    section.save
    assert section.errors[:days].present?
  end
end
