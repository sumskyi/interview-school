require 'test_helper'

class ClassroomTest < ActiveSupport::TestCase
  test 'valid fixture' do
    assert_valid_fixture classrooms
  end

  test 'validate name presence' do
    subject = Classroom.new name: ''
    subject.save
    assert subject.errors[:name].present?
  end

  test 'validate name uniqueness presence' do
    classrooms :one

    subject = Classroom.new name: 'MathRoom'
    subject.save

    assert subject.errors[:name].present?
  end
end
