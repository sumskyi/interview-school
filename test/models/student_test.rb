require 'test_helper'

class StudentTest < ActiveSupport::TestCase
  test 'valid fixture' do
    assert_valid_fixture students
  end

  test 'validate name presence' do
    subject = Student.new name: ''
    subject.save
    assert subject.errors[:name].present?
  end

  test 'validate name uniqueness presence' do
    students :putin

    subject = Student.new name: 'Putin'
    subject.save

    assert subject.errors[:name].present?
  end
end

