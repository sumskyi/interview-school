# frozen_string_literal: true

module Formtastic
  module Inputs
    module Base
      module Wrapping
        def input_wrapping(&block)
          tag = options.fetch(:wrapper_html, {}).delete(:tag) || 'li'
          template.content_tag(tag,
           [template.capture(&block), error_html, hint_html].join("\n").html_safe,
           wrapper_html_options
          )
        end

        def wrapper_classes
          classes = wrapper_classes_raw
          # classes << as
          # classes << "input"
          classes << "error" if errors?
          classes << "optional" if optional?
          classes << "required" if required?
          classes << "autofocus" if autofocus?

          classes.join(' ')
        end
      end
    end
  end
end

module Formtastic
  module Helpers
    module FormHelper
      @@default_form_model_class_proc = proc { nil }
    end
  end
end
