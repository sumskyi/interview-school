module SectionsHelper
  def options_for_days
    ::Section::HUMAN_DAYS.each_with_index.map do |day, idx|
      [day, idx]
    end
  end
end
