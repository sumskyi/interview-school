class Section < ApplicationRecord
  belongs_to :teacher
  belongs_to :classroom

  serialize :days, Array

  # see Tod::TimeOfDayType type
  attribute :starts_at, :time_only
  attribute :ends_at, :time_only

  validates :starts_at, :ends_at, presence: true
  validate :validate_days

  delegate :name, to: :classroom

  HUMAN_DAYS = %w[
    Monday
    Tuesday
    Wednesday
    Thursday
    Friday
    Saturday
    Sunday
  ]

  MINUTE = 60

  def starts_at; super && super.round(MINUTE); end
  def ends_at; super && super.round(MINUTE); end

  def to_s
    decorate.full_info
  end
  alias :name :to_s

  # TODO:
  # validate overlapping of section:
  #   - same teacher:   at same day and time
  #   - same classroom: at same day and time

  private

  def validate_days
    if !days.is_a?(Array) || days.detect{|d| !(0..6).include?(d)}
      errors.add(:days, :invalid)
    end
  end
end
