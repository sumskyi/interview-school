class Teacher < ApplicationRecord
  has_many :teacher_subjects
  has_many :sections
  has_many :subjects, through: :teacher_subjects
  accepts_nested_attributes_for :teacher_subjects, allow_destroy: true

  validates :first_name, :last_name, presence: true

  def first_and_last_name
    "#{first_name} #{last_name}"
  end
  alias_method :to_s, :first_and_last_name
end
