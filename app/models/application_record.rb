class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  ActiveRecord::Type.register(:time_only, Tod::TimeOfDayType)
end
