class Student < ApplicationRecord
  validates :name, uniqueness: true, presence: true

  def to_s; name; end
end
