class Classroom < ApplicationRecord
  has_many :sections
  validates :name, uniqueness: true, presence: true

  def to_s; name; end
end
