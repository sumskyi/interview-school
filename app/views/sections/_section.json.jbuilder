json.extract! section, :id, :teacher_id, :classroom_id, :days, :starts_at, :ends_at, :created_at, :updated_at
json.url section_url(section, format: :json)
