json.extract! schedule, :id, :student_id, :section_id, :created_at, :updated_at
json.url schedule_url(schedule, format: :json)
