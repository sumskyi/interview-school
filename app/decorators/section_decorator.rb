class SectionDecorator < Draper::Decorator
  delegate_all

  HUMAN_HM = '%H:%M'

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end
  def full_info
    "#{object.classroom.name}: #{days_human}, #{human_starts} - #{human_ends}"
  end

  def days_human
    Array(object.days)
      .sort
      .map { |day| object.class::HUMAN_DAYS[day - 1] }
      .join ' / '
  end

  def human_starts
    object.starts_at && object.starts_at.strftime(HUMAN_HM)
  end

  def human_ends
    object.ends_at && object.ends_at.strftime(HUMAN_HM)
  end
end
