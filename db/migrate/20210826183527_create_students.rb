class CreateStudents < ActiveRecord::Migration[6.0]
  def change
    create_table :students do |t|
      t.string :name, null: false

      t.timestamps
    end
    add_index :students, :name, unique: true
  end
end
