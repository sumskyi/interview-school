class CreateSections < ActiveRecord::Migration[6.0]
  def change
    create_table :sections do |t|
      t.references :teacher, null: false, foreign_key: { on_delete: :cascade }
      t.references :classroom, null: false, foreign_key: { on_delete: :cascade }
      t.string :days
      t.string :starts_at
      t.string :ends_at

      t.timestamps
    end
  end
end
