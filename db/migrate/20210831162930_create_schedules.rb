class CreateSchedules < ActiveRecord::Migration[6.0]
  def change
    create_table :schedules do |t|
      t.references :student, null: false, foreign_key: { on_delete: :cascade }
      t.references :section, null: false, foreign_key: { on_delete: :cascade }

      t.timestamps
    end
    add_index :schedules, [:student_id, :section_id], unique: true
  end
end
